# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-phone-components package.
#
# A S Alam <alam.yellow@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: plasma-phone-components\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2019-10-31 03:02+0100\n"
"PO-Revision-Date: 2019-12-29 09:25-0800\n"
"Last-Translator: A S Alam <alam.yellow@gmail.com>\n"
"Language-Team: Punjabi <punjabi-users@lists.sf.net>\n"
"Language: pa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.3\n"

#: package/contents/ui/ConfigOverlay.qml:102
#, kde-format
msgid "Remove"
msgstr "ਹਟਾਓ"